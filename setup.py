from setuptools import setup, find_packages

VERSION = '1.0'

setup(
        name='drunkwalk-simulator',
        version=VERSION,
        packages=find_packages(),
        entry_points={
            'console_scripts': [
                'drunkwalk-simulator = system.__main__:run',
            ],
        },
        install_requires=[
            'matplotlib==2.0.2',
            'numpy==1.12.1',
            'Pillow==4.1.1',
            'PyQt5==5.8.2',
            'Sphinx==1.6.2',
            'sphinx-rtd-theme==0.2.4',
        ],
)
