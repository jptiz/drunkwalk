BASEDIR=$(dirname $0)

echo Building docs for "$BASEDIR"
sphinx-apidoc -o doc_build "$BASEDIR"/../simulator -F -H 'simulator' -A 'João Paulo Taylor Ienczak Zanette'
cp "$BASEDIR"/conf.py doc_build
cd doc_build
make latex
