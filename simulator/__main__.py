#!/usr/bin/python3
'''Simulation main execution module. Starts GUI version.'''
from simulator import gui

if __name__ == '__main__':
    gui.SimulationWindow().run()
