#!/usr/bin/python3
'''GUI execution of DrunkWalk simulation.'''

import sys
import time
import numpy as np
from math import sqrt, ceil

import matplotlib
matplotlib.use('Qt5Agg') # noqa
from matplotlib.backends.backend_qt5agg import (
        FigureCanvasQTAgg as FigureCanvas)
from matplotlib.figure import Figure

from simulator import simulation
from .simulation import Point
from .simulation import Parameters

from PIL import Image, ImageDraw, ImageFont
from PyQt5.QtWidgets import (QApplication, QMainWindow,
                             QGridLayout, QLabel, QPushButton, QSpinBox,
                             QProgressBar, QWidget, QTabWidget, QSizePolicy)
from PyQt5.QtGui import QImage, QPixmap


GRAY = (128, 128, 128, 255)


def as_qt_image(image):
    '''Converts `image` to a valid QImage from Qt.'''
    return QImage(
            image.convert('RGBA').tobytes('raw', 'RGBA'),
            image.size[0], image.size[1], QImage.Format_RGBA8888)


def draw_grid(image, step=1):
    '''Draws a grid in given image.

    Args:
        step (int): Step between gridlines.
    '''
    draw = ImageDraw.Draw(image)

    right = Point(*image.size)
    middle = right * 0.5

    draw.line(((middle.x, 0), (middle.x, right.y)), fill=GRAY)
    draw.line(((0, middle.y), (right.x, middle.y)), fill=GRAY)

    font = ImageFont.truetype('Roboto-Regular.ttf', size=9)
    for i in range(1, int(middle.x / step)):
        i_ = i * step
        d = 10 * i_

        # Horizontal gridline
        draw.line((middle - Point(d, 2), middle + Point(-d, 2)), fill=GRAY)
        draw.text(middle + Point(-d, 2), '-{}'.format(i_), fill=GRAY,
                  font=font)

        draw.line((middle + Point(d, -2), middle + Point(d, 2)), fill=GRAY)
        draw.text(middle + Point(d, 2), '{}'.format(i_), fill=GRAY, font=font)

        # Vertical gridline
        draw.line((middle - Point(2, d), middle + Point(2, -d)), fill=GRAY)
        draw.text(middle + Point(2, -d), '-{}'.format(i_), fill=GRAY,
                  font=font)

        draw.line((middle + Point(-2, d), middle + Point(2, d)), fill=GRAY)
        draw.text(middle + Point(2, d), '{}'.format(i_), fill=GRAY, font=font)

    return image


class PlotCanvas(FigureCanvas):
    '''Canvas for graphic plotting.'''

    def __init__(self, parent=None, size=(5, 4), dpi=100):
        '''Constructs PlotCanvas.

        Args:
            size (tuple): Plot size (to be multiplied by dpi).
            dpi (int): Dots per Inch.
        '''
        figure = Figure(figsize=size, dpi=dpi, linewidth=1.0)
        self.axes = figure.add_subplot(111)
        FigureCanvas.__init__(self, figure)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(
                self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)

        FigureCanvas.updateGeometry(self)

    def clear(self):
        '''Clears plot content.'''
        self.figure.clear()

    def plot(self, data):
        '''Plots data.

        Args:
            data (sequence): data to be plotted. `tuple` may be used to pass
                             `matplotlib`'s `Figure.plot(...)` arguments.
        '''
        hist = self.figure.add_subplot(111, xmargin=0, ymargin=0)
        if isinstance(data, tuple):
            hist.plot(*data)
        else:
            hist.plot(data)
        self.draw()


class HistogramCanvas(PlotCanvas):
    '''Canvas specifically for histogram drawing.'''

    def __init(self, parent=None):
        '''Constructs HistogramCanvas.

        Args:
            parent: parent element.
        '''
        PlotCanvas.__init__(self, parent=parent)

    def plot(self, data, bins=30):
        '''Plots histogram data.

        Args:
            data (list): the data.
            bins (int): bars count.
        '''
        plot_ = self.figure.add_subplot(111, xmargin=0, ymargin=0)
        hist, bins = np.histogram(data, bins=ceil(min(bins, 30)))
        width = 0.9 * (bins[1] - bins[0])
        center = (bins[:-1] + bins[1:]) / 2
        plot_.bar(center, hist, align='center', width=width)
        self.draw()


class SimulationWindow():
    '''Window for simulation view and parameter changing.'''

    def __init__(self):
        '''Constructs the simulation window.'''

        self.app = QApplication(sys.argv)

        self.window = QMainWindow()
        self.window.resize(800, 600)
        self.window.setWindowTitle('Simulação: O Andar do Bêbado')

        self.centralWidget = QWidget()
        self.window.setCentralWidget(self.centralWidget)

        self.progressBar = QProgressBar(self.window)
        self.progressBar.setMaximumSize(200, 24)
        self.progressBar.setValue(100)

        self.window.statusBar()
        self.window.statusBar().showMessage('Pronto.')
        self.window.statusBar().addPermanentWidget(self.progressBar)

        grid = QGridLayout(self.centralWidget)
        grid.setSpacing(10)

        n_label = QLabel('Número de passos:')

        self.n_spin = QSpinBox()
        self.n_spin.setMinimum(1)
        self.n_spin.setMaximum(10000000)
        self.n_spin.setValue(100)
        self.n_spin.setSuffix(' passos')
        self.n_spin.setSpecialValueText('1 passo')

        l_label = QLabel('Distância máxima do passo:')
        l_label.resize(144, 28)

        self.l_spin = QSpinBox()
        self.l_spin.setMinimum(1)
        self.l_spin.setSuffix('m')
        self.l_spin.setSpecialValueText('1m')

        executions_label = QLabel('Número de execuções:')
        executions_label.resize(124, 28)

        self.executions_spin = QSpinBox()
        self.executions_spin.setMinimum(1)
        self.executions_spin.setMaximum(10000000)
        self.executions_spin.setValue(100)
        self.executions_spin.setSuffix(' execuções')
        self.executions_spin.setSpecialValueText('1 execução')

        self.mean_deviation_label = QLabel('')
        self.mean_deviation_label.resize(124, 28)

        self.expected_distance_label = QLabel('')
        self.expected_distance_label.resize(124, 28)

        run_button = QPushButton('Executar')
        run_button.clicked.connect(self.run_simulation)

        self.image_container = QLabel()
        self.distance_graphic = PlotCanvas()
        self.histogram = HistogramCanvas()

        self.content_pane = QTabWidget(self.window)
        self.content_pane.addTab(self.image_container,
                                 'Caminho da última execução')
        self.content_pane.addTab(self.distance_graphic, 'Distância/passo')
        self.content_pane.addTab(self.histogram, 'Histograma')

        grid.addWidget(n_label, 0, 0)
        grid.addWidget(self.n_spin, 0, 1)
        grid.addWidget(executions_label, 1, 0)
        grid.addWidget(self.executions_spin, 1, 1)
        grid.addWidget(l_label, 2, 0)
        grid.addWidget(self.l_spin, 2, 1)
        grid.addWidget(self.mean_deviation_label, 0, 2)
        grid.addWidget(self.expected_distance_label, 1, 2)
        grid.addWidget(run_button, 2, 2)
        grid.addWidget(self.content_pane, 3, 0, 1, 8)

        self.run_simulation()

    def update_graphics(self, params, results):
        '''Update simulation graphics.

        Args:
            params (simulation.Parameters): Simulation parameters.
            results (simulation.Results): Simulation result data.
        '''

        step_length = params.step_length
        image = Image.new('RGB', (800, 600), (255, 255, 255))
        draw_grid(image, step=step_length)
        self.image = simulation.draw_path(image, results.path)
        self.image = as_qt_image(self.image)

        pixmap = QPixmap.fromImage(self.image)
        self.image_container.setPixmap(pixmap)

        x = np.arange(0, len(results.distances), step=0.1)
        y = step_length * np.sqrt([value for value in x])

        self.distance_graphic.clear()
        self.distance_graphic.plot(data=results.distances)
        self.distance_graphic.plot(data=(x, y, 'r-'))

        self.histogram.clear()
        self.histogram.plot(
                data=[(d - step_length*sqrt(i))/len(results.distances)
                      for i, d in enumerate(results.distances)],
                bins=sqrt(params.executions))

    def run_simulation(self):
        '''Runs simulation. Parameters come from UI elements.'''
        start_time = time.time()

        params = Parameters(
            steps=self.n_spin.value(),
            step_length=self.l_spin.value(),
            executions=int(self.executions_spin.value()))

        def update_status(i):
            '''Updates status bar.

            Args:
                i (int): Current execution index (starting in 0).
            '''
            self.window.statusBar().showMessage(
                    'Gerando caminho {} de {}...'.format(i + 1,
                                                         params.executions))
            self.progressBar.setValue(round(100 * i / params.executions))

        results = simulation.run(params, update_status)

        self.window.statusBar().showMessage(
                'Simulação efetuada com sucesso. Gerando gráficos...')
        self.update_graphics(params=params, results=results)

        mean_deviation = 0
        for i, distance in enumerate(results.distances):
            mean_deviation += distance - params.step_length*sqrt(i)
        mean_deviation /= params.steps

        self.mean_deviation_label.setText(
                'Média do desvio: {:.2f}'.format(mean_deviation))

        expected_distance = params.step_length * sqrt(params.steps)
        self.expected_distance_label.setText(
                'Distância esperada: {:.2f}'.format(expected_distance))

        elapsed = time.time() - start_time
        self.window.statusBar().showMessage(
                'Pronto ({:.2f}s).'.format(elapsed))
        self.progressBar.setValue(100)

    def run(self):
        '''Runs Qt application and shows window.'''
        self.window.show()
        self.app.exec_()
