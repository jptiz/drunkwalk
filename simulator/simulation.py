#!/usr/bin/python3
'''Drunk's Walk Simulation's core.'''

from typing import NamedTuple
from math import sin, cos, sqrt, radians
from PIL import ImageDraw, ImageFont
import numpy.random


class Point(NamedTuple):
    '''Point in 2D space.'''

    x: float
    y: float

    def __mul__(self, scalar):
        '''Multiplies a point with a scalar.'''
        return Point(self.x * scalar, self.y * scalar)

    def __add__(self, point):
        '''Adds point with another one.'''
        return Point(self.x + point.x, self.y + point.y)

    def __sub__(self, point):
        '''Subtracs point with another one.'''
        return Point(self.x - point.x, self.y - point.y)


class Results:
    '''Contains simulation result data.'''

    def __init__(self, path, distances):
        '''Constructs simulation results.

        Args:
            path (list of Point): Last execution's path.
            distances (list): Distance walked for each step.
        '''
        self.path = path
        self.distances = distances


class Parameters(NamedTuple):
    '''Contains parameters used for simulation.'''
    steps: int
    step_length: float
    executions: int


def generate_path(n, l=1):
    '''Generates the walked path. Returns the path and the distances from the
    start to each point in path.

    Args:
        n (int): Number of steps in path.
        l (int): Length of each step.
    '''
    x, y = 0, 0
    path = [Point(x, y)]
    d = []
    for i in range(n):
        alpha = radians(numpy.random.uniform(0.0, 360.0))

        x = x + l*cos(alpha)
        y = y + l*sin(alpha)

        path.append(Point(x, y))
        d.append(sqrt(x*x + y*y))
    return path, d


def draw_path(image, path, color=(255, 0, 0, 255)):
    '''Draws path on image.

    Args:
        image (PIL.Image): Image in which path will be drawn.
        path (list of Point): The path.
        color (tuple): Path's color.
    '''
    draw = ImageDraw.Draw(image)
    middle = Point(*image.size) * 0.5

    transformed = [p * 10 + middle for p in path]

    draw.line(transformed, fill=(255, 0, 0, 255))
    font = ImageFont.truetype('Roboto-Regular.ttf', size=24)

    start, *_, end = path

    draw.line((start * 10 + middle, end * 10 + middle),
              fill=(0, 128, 255, 255))

    x, y = end - start

    d = sqrt(x*x + y*y)

    draw.text((0, 0), 'Distância: {:.2f} (diferença: {:.2f})'.format(
              d, d - sqrt(len(path))), fill=(0, 0, 0, 255), font=font)

    return image


def run(params, callback=None):
    '''Runs simulation.

    Args:
        params (Parameters): Simulation parameters.
        callback (function): Function to be called after every processed path.
    '''
    steps, step_length, executions = params
    path = [(0, 0), (0, 0)]
    d = [0] * (steps + 1)

    for i in range(executions):
        if callback is not None:
            callback(i)
        path, distance = generate_path(steps, step_length)

        for j in range(steps):
            d[j + 1] += distance[j]

        start, *_, end = path
        x, y = end

    callback(i)

    for i in range(steps):
        d[i + 1] /= executions

    return Results(path, d)
