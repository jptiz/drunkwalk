#!/usr/bin/python3
'''Module for cli simulation. Not used by GUI nor main.py'''

from math import sqrt

from simulator import simulation


if __name__ == '__main__':
    n = 100
    l = 1
    executions = 1

    d = 0
    for i in range(executions):
        path = simulation.generate_path(n=n, l=l)
        image = simulation.image_from_path(path)
        image.save('drunks_path.png')

        start, *_, end = path
        x, y = end - start
        d += sqrt(x*x + y*y)
    print('mean distance: {}'.format(d / executions))
    print('expected sqrt(n):  {}'.format(sqrt(n)))
