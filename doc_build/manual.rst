Manual de Uso
=============

Executar o Simulador
--------------------

Para executar o simulador, basta executar o arquivo "main.exe" com um duplo
clique (ou selecioná-lo e pressionar a tecla "Enter").

Tela Principal
--------------

.. _mainwindow:
.. figure:: main_window.png
    :align: center
    :alt: Tela principal do programa.

    Tela principal do simulador.

Ao abrir o programa, uma simulação é executada e seus resultados são
apresentados na janela representada na :numref:`mainwindow`. Essa simulação é
executada considerando os parâmetros padrões, que são:

* **Número de Passos:** 100
* **Número de execuções:** 100
* **Tamanho do passo:** 1m

Há também, ao lado das configurações dos parâmetros, marcadores indicando:

* **Média do desvio:** Representa a média dentre todos os desvios em relação à
  distância esperada.
* **Distância esperada:** A distância esperada em um caso ideal. É dada pela
  raiz quadrada do número de passos (multiplicada pela distância de cada
  passo).


Gráficos
--------

É exibido constantemente um dos três gráficos: Caminho da última execução,
Distância/passo e Histograma.

Caminho da última execução
^^^^^^^^^^^^^^^^^^^^^^^^^^

Para *n* execuções, o caminho percorrido na *n-ésima* execução é exibido. Nesse
gráfico, linhas vermelhas indicam passos dados pelo bêbado, e uma linha azul
indica o caminho resultante do ponto de partida (que sempre está na origem) até
o ponto final (onde o bêbado parou).

Distância/passo
^^^^^^^^^^^^^^^

.. _distgraph:
.. figure:: dist_graph.png
    :align: center
    :alt: Gráfico de Distância/passo.

    Gráfico de distância média percorrida por passo.

Exibe a distância média percorrida a cada passo (:numref:`distgraph`), em uma
sequência de 0 a *n* passos (demarcada por uma linha azul) em comparação ao
valor esperado (:math:`\sqrt{n}`, demarcado por uma linha vermelha).

Histograma
^^^^^^^^^^

.. _histogram:
.. figure:: histogram.png
    :align: center

    Histograma de execuções.

Exibe um histograma (:numref:`histogram`) simbolizando o desvio da distância
percorrida a cada passo em relação ao valor esperado, agrupados em até 30
classes.

O agrupamento é dado por:

.. code-block:: python

    classes = min(30, sqrt(executions))

Em que ```executions``` é o número de replicações a serem feitas na simulação.


Definindo parâmetros de simulação
---------------------------------

Como alterar parâmetros
^^^^^^^^^^^^^^^^^^^^^^^

Para definir os parâmetros de execução, basta alterar as três *SpinBoxes*
(campos de edição de número) no topo da janela, em que:

* **Número de passos:** Indica quantos passos será dado em cada
  execução/replicação da simulação.
* **Número de execuções:** Indica quantas replicações serão feitas na
  simulação. Por exemplo, 50 passos com 100 execuções indica que o simulador
  irá fazer 100 simulações de um bêbado dando 50 passos a partir da origem.
* **Distância máxima do passo:** Indica o comprimento de cada passo (padrão: 1
  metro). Assim, 50 passos de 1m fará o bêbado andar 50x1m = 50 metros. O
  resultado de um valor mais alto pode ser visto na :numref:`biggerstep`.


.. _biggerstep:
.. figure:: higher_step.png
    :align: center

    Exemplo de um caminho percorrido com passo maior que o padrão.

Comportamento da simulação
^^^^^^^^^^^^^^^^^^^^^^^^^^

Conforme o resultado desejado para a simulação, diferentes parâmetros gerarão
comportamentos diferentes. Assim sendo:

* Maior número de passos torna mais fácil visualizar a convergência em relação
  ao valor esperado, aumentando a amostragem de dados.
* Maior número de execuções torna mais precisa a convergência em relação ao
  valor esperado. Um exemplo de valor baixo de execuções (ou seja, pouca
  convergência) pode ser visto na :numref:`fewruns`;


.. _fewruns:
.. figure:: low_runs.png
    :align: center

    Exemplo de simulação com poucas execuções (25).


Executar simulação
------------------

Para executar a simulação, dados os parâmetros desejados, basta clicar no botão
"Executar".

.. _processing:
.. figure:: processing.png
    :align: center

    Processo de simulação em andamento.

Ao executar uma simulação (:numref:`processing`), é possível acompanhar na
barra de status quantas replicações foram completadas (com o adicional de uma
barra de carregamento acompanhada da porcentagem efetuada da simulação).

Ao final das replicações, os três gráficos serão gerados, sendo informado na
barra de status enquanto a simulação estiver nessa etapa.

Quando a simulação for finalizada, a barra de status exibirá a mensagem
"Pronto.", seguido do tempo decorrido de simulação.



